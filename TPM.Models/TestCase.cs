﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPM.Models
{
    public class TestCase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Requirement { get; set; }
        public string Description { get; set; }
        public string User { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<TestStep> TestSteps { get; set; }
    }
}
