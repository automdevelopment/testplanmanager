﻿using System;
using System.Collections.Generic;

namespace TPM.Models
{
    public class TestPlan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TestCase> TestCases { get; set; }
    }
}
