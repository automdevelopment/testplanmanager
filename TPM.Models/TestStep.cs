﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPM.Models
{
    public class TestStep
    {
        public int Id { get; set; }
        public int OrderNumber { get; set; }
        public string Description { get; set; }
        public string Result { get; set; }
        public string Status { get; set; }
    }
}
