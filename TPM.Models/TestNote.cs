﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPM.Models
{
    public class TestNote
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
    }
}
